# Script based on
# https://github.com/abdulmoizeng/crawlers-demo/blob/master/crawler-demo/spider2.py

import requests, argparse, json, sys, tempfile, os
from parsel import Selector
from furl import furl

import time

def parse_cli_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-u', '--url', nargs='+', help='<Required> urls to crawl.', required=True
    )
    parser.add_argument(
        '-o', '--output-yaml-file', help='Targed yaml file.'
    )
    parser.add_argument(
        '--ignore-urls-with-args',
        help='Ignore any urls with args (?foo&bar).',
        action='store_true'
    )
    parser.add_argument(
        '--ignore-urls-with-fragments',
        help='Ignore any urls with fragments (#baz).',
        action='store_true'
    )
    parser.add_argument(
        '--only-external-images',
        help='Only track images hosted elsewhere.',
        action='store_true'
    )
    parser.add_argument(
        '--output-pages-without-images',
        help='Include searched adresses without images in output.',
        action='store_true'
    )
    return parser.parse_args()

def url_has_fragments(url):
    if furl(url).fragment:
        return True
    return False

def url_has_args(url):
    if furl(url).args:
        return True
    return False

def only_external_images(url, img_links):
    result = list()
    for img_link in img_links:
        f = furl(img_link)
        if not f.scheme:
            continue
        if not f.host:
            continue
        if furl(url).host == f.host:
            continue
        result.append(img_link)
    return result

def url_host_path(url):
    return str(furl(url).host) + str(furl(url).path)

def crawl_site_set_initial_kwargs_values(url, kwargs):
    if not kwargs.get('original_url'):
        kwargs['original_url'] = url
    if not kwargs.get('checked_urls'):
        kwargs['checked_urls'] = set()
    if not kwargs.get('image_index'):
        kwargs['image_index'] = dict()
    if not kwargs.get('ignore_urls_with_args'):
        kwargs['ignore_urls_with_args'] = False
    if not kwargs.get('ignore_urls_with_fragments'):
        kwargs['ignore_urls_with_fragments'] = False
    if not kwargs.get('only_external_images'):
        kwargs['only_external_images'] = False
    if not kwargs.get('output_pages_without_images'):
        kwargs['output_pages_without_images'] = False
    return kwargs

def crawl_site(url, **kwargs):

    try:
        safe_url = furl(url).url
    except ValueError:
        return kwargs

    # Initial values.
    kwargs = crawl_site_set_initial_kwargs_values(safe_url, kwargs)

    # Validation if we can continue recursive calls.
    if safe_url in kwargs['checked_urls']:
        return kwargs
    else:
        # Save url as checked so we don't retry it again later.
        kwargs['checked_urls'].add(safe_url)

    # More validation if we can continue recursive calls.
    if not url_host_path(safe_url).startswith(url_host_path(kwargs['original_url'])):
        return kwargs
    if kwargs['ignore_urls_with_args'] and url_has_args(safe_url):
        return kwargs
    if kwargs['ignore_urls_with_fragments'] and url_has_fragments(safe_url):
        return kwargs

    # Handle urls we can't read.
    try:
        response = requests.get(safe_url)
    except e:
        raise e
    if not response.status_code == 200:
        return kwargs

    selector = Selector(response.text)
    img_links = selector.xpath('//img/@src').getall()
    if kwargs['only_external_images']:
        img_links = only_external_images(safe_url, img_links)
    if kwargs['output_pages_without_images']:
        kwargs['image_index'][url] = img_links
    elif img_links:
        kwargs['image_index'][url] = img_links

    href_links = selector.xpath('//a/@href').getall()

    for link in href_links:
        if link.startswith('/') and not furl(link).host:
            full_link = furl(safe_url)
            path_link = furl(link)
            full_link.args = path_link.args
            full_link.path = str(path_link.path)
            full_link.fragment = str(path_link.fragment)
            kwargs = crawl_site(full_link.url, **kwargs)
        else:
            kwargs = crawl_site(link, **kwargs)

    return kwargs

def update_json_file(output_file, data):
    existing_data = None
    with open(output_file, 'r') as of:
        try:
            existing_data = json.load(of)
        except json.decoder.JSONDecodeError:
            existing_data = dict()
    existing_data.update(data)
    with open(output_file, 'w') as of:
        json.dump(existing_data, of, indent=4, sort_keys=True)
    return existing_data

if __name__ == "__main__":
    args = parse_cli_arguments()
    urls = set(args.url)
    _, work_file = tempfile.mkstemp(prefix='scrape.py')
    verbose = False
    if args.output_yaml_file:
        verbose = True
    for url in urls:
        start = time.time()
        crawl_result = crawl_site(
            url,
            ignore_urls_with_args=args.ignore_urls_with_args,
            ignore_urls_with_fragments=args.ignore_urls_with_fragments,
            only_external_images=args.only_external_images,
            output_pages_without_images=args.output_pages_without_images
        )
        end = time.time()
        data = {
            url: {
                'seconds': (end-start),
                'images': crawl_result['image_index']
            }
        }
        update_json_file(work_file, data)
        if verbose:
            status = "Completed scraping of {url} in {seconds} seconds."
            print(status.format(url=url, seconds=data[url]['seconds']))
    result = None
    with open(work_file) as wf:
        result = json.load(wf)
    os.remove(work_file)
    if args.output_yaml_file:
        with open(args.output_yaml_file, 'w') as oyf:
            json.dump(result, oyf, indent=4, sort_keys=True)
    else:
        print(json.dumps(result, indent=4, sort_keys=True))
